const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');

const user = require('./app/user');
const gallery = require('./app/gallery');

const config = require('./config');


const port = 8000;
const app = express();
app.use(cors());
app.use(express.json());
app.use(express.static('public'))


mongoose.connect(config.dbUrl, config.mongoOptions).then(()=>{
  console.log('MongoDB Started!');

  app.use('/user', user);
  app.use('/gallery', gallery);
  app.listen(port, ()=>{
    console.log(`Server started on ${port} port`)
  })
}).catch(()=>{console.log('MongoDB failure')});