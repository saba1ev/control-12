const mongoose = require('mongoose');
const nanoid = require('nanoid');
const bcrypt =require('bcrypt');

const Schema = mongoose.Schema;

const SALT_WORK_FACTOR = 10;

const UserSchema = new Schema({
  username: {
    type: String,
    required: true,
    unique: true,
    validate: {
      validator: async function (value) {
        if (!this.isModified('username')) return;

        const user = await UserModule.findOne({username: value});

        if (user) throw new Error();
      },
      message: 'This username is already token'
    }
  },
  password: {
    type: String,
    required: true
  },
  nickname: {
    type: String,
    required: true
  },
  token: {
    type: String,
    required: true
  },
  facebookId: {
    type: String,
  }
});
UserSchema.methods.checkPassword = function (password) {
  return bcrypt.compare(password, this.password)
};
UserSchema.methods.generateToken = function () {
  return this.token = nanoid();
};
UserSchema.pre('save', async function (next) {
  if (!this.isModified('password')) return next();

  const salt = await bcrypt.genSalt(SALT_WORK_FACTOR);
  const hash = await bcrypt.hash(this.password, salt);

  this.password = hash;

  next();
});

UserSchema.set('toJSON', {
  transform: (doc, ret, options) => {
    delete ret.password;
    return ret;
  }
});

const UserModule = mongoose.model('User', UserSchema);

module.exports = UserModule;