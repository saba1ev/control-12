const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const GallerySchema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  title: {
    type: String,
    required: true
  },
  photo: {
    type: String,
    required: true
  }
});
const GalleryModule = mongoose.model('Gallery', GallerySchema);

module.exports = GalleryModule;