const nanoid = require("nanoid");
const mongoose = require('mongoose');

const User = require('./models/User');
const Gallery = require('./models/Gallery');
const config = require('./config');

const run = async () => {
  await mongoose.connect(config.dbUrl, config.mongoOptions);

  const connection = mongoose.connection;

  const collections = await connection.db.collections();

  for (let collection of collections) {
    await collection.drop();
  }

  const user = await User.create({
      username: 'admin',
      token: nanoid(),
      password: nanoid(),
      nickname: 'Admin',
      facebookId: '2533957180016540',
    },
    {
      username: 'sobolev',
      token: nanoid(),
      password: '1234',
      nickname: 'sobolez',
    }
  );
  await Gallery.create(
    {
      user: user[1]._id,
      title: 'Hello World!',
      photo: '1.jpeg'
    },
    {
      user: user[1]._id,
      title: 'Picture',
      photo: '2.jpg'
    },
    {
      user: user[0]._id,
      title: 'Bla bla bla',
      photo: '3.jpeg'
    },
    {
      user: user[0]._id,
      title: 'It\'s wonderful',
      photo: '4.jpeg'
    },
    {
      user: user[0]._id,
      title: 'It\'s amazing',
      photo: '5.png'
    },
    {
      user: user[1]._id,
      title: 'Some text',
      photo: '6.jpg'
    }

  );


  return connection.close();
};


run().catch(error => {
  console.log('Something wrong happened ...', error);
});