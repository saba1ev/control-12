const express = require('express');
const multer = require('multer');
const nanoid = require('nanoid');
const config = require('../config');
const path = require('path');
const gallery = require('../models/Gallery');
const auth = require('../middleware/auth');
const tryAuth = require('../middleware/tryAuth');

const storage = multer.diskStorage({
  destination: (req, file, cb)=>{
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) =>{
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const router = express.Router();

router.get('/',  (req, res)=>{
  gallery.find().populate('user')
    .then(result => res.send(result))
    .catch(() => res.sendStatus(500))
});
router.get('/:id', (req, res)=>{
  gallery.find({user:req.params.id})
    .then(result => res.send(result))
    .catch(err => res.status(500).send(err))
});

router.post('/', [auth, upload.single('photo')], (req, res)=>{
  const galleryData = new gallery({...req.body, user: req.user._id});
  if (req.file){
    galleryData.photo = req.file.filename;
  }
  const Gallery = new gallery(galleryData);
  Gallery.save()
    .then(result=> res.send(result))
    .catch(err => res.status(400).send(err))
});
router.delete('/:id', auth, async (req, res)=>{
  gallery.deleteOne({_id: req.params.id})
    .then(result => res.send(result))
    .catch(err => res.status(403).send(err))
});


module.exports = router;