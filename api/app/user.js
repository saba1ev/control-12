const express = require('express');
const axios = require('axios');
const User = require('../models/User');
const auth = require('../middleware/auth');
const config = require('../config');
const nanoid = require('nanoid');


const router = express.Router();


router.post('/', async (req, res) => {
  const user = new User(req.body);
  user.generateToken();

  try {
    await user.save();
    return res.send({token: user.token, nickname: user.nickname, _id: user._id});
  } catch (e) {
    return res.status(400).send(e)
  }
});

router.post('/sessions', async (req, res) => {
  const user = await User.findOne({username: req.body.username});

  if (!user) {
    return res.status(400).send({error: `Username does not exist` });
  }

  const isMatch = await user.checkPassword(req.body.password);

  if (!isMatch) {
    return res.status(400).send({error: `Password is wrong`});
  }

  user.generateToken();
  await user.save();

  res.send({nickname: user.nickname, token: user.token, _id: user._id});
});
router.delete('/sessions', async (req, res) => {
  const token = req.get('Token');
  const success = {message: 'Logged out'};

  if (!token) {
    return res.send(success);
  }

  const user = await User.findOne({token});

  if (!user) {
    return res.send(success);
  }

  user.generateToken();
  await  user.save();

  return res.send(success);
});


router.put('/', auth, async (req, res) => {

  req.user.password = req.body.password;

  await req.user.save();

  res.sendStatus(200);
});
router.post('/facebookLogin', async (req,res)=>{
  const inputToken = req.body.accessToken;
  const accessToken = config.facebook.appId + '|' + config.facebook.appSecret;
  const debugTokenUrl = `https://graph.facebook.com/debug_token?input_token=${inputToken}&access_token=${accessToken}`;

  try {
    const response = await axios.get(debugTokenUrl);
    const responseData = response.data;
    if (responseData.data.error){
      return res.status(500).send({error: 'Token incorrect'})
    }
    if (responseData.data.user_id !== req.body.id){
      return res.status(500).send({error: 'User is wrong'})
    }
    let user = await User.findOne({facebookId: req.body.id});
    if (!user){
      user = new User({
        username: req.body.email || req.body.id,
        password: nanoid(),
        facebookId: req.body.id,
        nickname: req.body.name,
      });
    }

    user.generateToken();
    await user.save();
    return res.send({message: 'Login or Register successful!', user})

  }catch (e) {
    return res.status(500).send({error: 'Something went wrong'});
  }
});


module.exports = router;