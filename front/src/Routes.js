import React from 'react';
import {Route, Switch, Redirect} from "react-router-dom";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import HomePage from "./containers/HomePage/HomePage";
import GalleryAuthor from "./containers/GalleryAuthor/GalleryAuthor";
import addImage from "./containers/addImage/addImage";




const ProtectedRoute = ({isAllowed, ...props}) => {
  return isAllowed ? <Route {...props}/> : <Redirect to="/"/>;
};

const Routes = ({user}) => {
  return (
    <Switch>
      <Route path="/" exact component={HomePage}/>
      <Route path="/register" exact component={Register}/>
      <Route path="/login" exact component={Login}/>
      <Route path='/gallery/:id' component={GalleryAuthor}/>
      <ProtectedRoute
        isAllowed={user}
        path="/add_image"
        exact
        component={addImage}
      />
      <ProtectedRoute
        isAllowed={user}
        path="/user_gallery"
        exact
        // component={MyCocktail}
      />
    </Switch>
  );
};

export default Routes;