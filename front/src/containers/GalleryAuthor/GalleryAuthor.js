import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {fetchAuthorGallery} from "../../store/actions/galleryActions";
import GalleryThumbnail from "../../components/GalleryThumbnail/GalleryThumbnail";
import {Button, Card, CardBody, CardTitle, NavLink} from "reactstrap";
import {NavLink as RouterNavLink} from "react-router-dom";

class GalleryAuthor extends Component {
  componentDidMount(){
    this.props.fetchAuthorGallery(this.props.match.params.id)
  }
  render() {
    return (
      <Fragment>
        {this.props.author ? this.props.author.map(item=>(
          <Card key={item._id}>
            <GalleryThumbnail image={item.photo}/>
            <CardTitle>
              {item.title}
            </CardTitle>
            <CardBody>
              <div>
                <p className='body-author'>publish by: </p>
                <p>{item.user.nickname}</p>
              </div>
            </CardBody>
          </Card>

        )):null}
      </Fragment>
    );
  }
}
const mapStateToProps = state => ({
  author: state.gallery.author
});

const mapDispatchToProps = dispatch => ({
  fetchAuthorGallery: (id) => dispatch(fetchAuthorGallery(id))
});

export default connect(mapStateToProps, mapDispatchToProps) (GalleryAuthor);