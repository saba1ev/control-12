import React, {Component} from 'react';
import FormElement from "../../components/UI/Form/FormElement";
import {Button, Col, Form, FormGroup, Input, Label} from "reactstrap";
import {connect} from "react-redux";
import {postGallery} from "../../store/actions/galleryActions";

class AddImage extends Component {
  state = {
    title: '',
    photo: ''
  };
  changeInputHendler = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  changeFileHendler = event => {
    this.setState({
      [event.target.name]: event.target.files[0]
    })
  };
  submitFormHandler = event => {
    event.preventDefault();

    const formData = new FormData();

    Object.keys(this.state).forEach(key => {
      formData.append(key, this.state[key]);
    });
    this.props.postImage(formData);
  };
  render() {
    return (
      <Form onSubmit={this.submitFormHandler}>
        <FormElement
          propertyName='title'
          type='text'
          title='Title' value={this.state.title}
          onChange={this.changeInputHendler}/>
        <FormGroup row>
          <Label sm={2} for="photo">Photo</Label>
          <Col sm={10}>
            <Input
              type="file"
              name="photo" id="photo"
              onChange={this.changeFileHendler}
            />
          </Col>
        </FormGroup>
        <Button type='submit'>Add</Button>
      </Form>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user.user
});
const mapDispatchToProps = dispatch => ({
  postImage: (id) => dispatch(postGallery(id))
});

export default connect(mapStateToProps, mapDispatchToProps) (AddImage);