import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {deletePhoto, fetchGallery} from "../../store/actions/galleryActions";
import {
  Button,
  Card,
  CardBody,
  CardColumns,
  CardTitle,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  NavLink
} from "reactstrap";
import GalleryThumbnail from "../../components/GalleryThumbnail/GalleryThumbnail";
import {NavLink as RouterNavLink} from 'react-router-dom';

class HomePage extends Component {
  state = {
    modal: null
  };
  openModal = (item) => {
    this.setState({modal: item})
  };
  closeModal = () => {
    this.setState({modal: null})
  };

  componentDidMount(){
    this.props.fetchGallery()
  }
  render() {
    return (
      <Fragment>

        <CardColumns>
          {this.props.user && this.props.gallery ? this.props.gallery.map(item=>(
            <Card key={item._id} onClick={()=>this.openModal(item)}>
              <GalleryThumbnail image={item.photo}/>
              <CardTitle>
                {item.title}
              </CardTitle>
              <CardBody>
                <div>
                  <p className='body-author'>publish by: </p>
                  <NavLink tag={RouterNavLink}
                           className='body-author'
                           to={`/gallery/${item.user._id}`}
                  >
                    {item.user.nickname}
                    </NavLink>
                  {item.user._id  !== this.props.user._id ? null :
                    <Button onClick={()=>this.props.deletePhoto(item._id)}>Delete</Button>}
                </div>
              </CardBody>
            </Card>
          )): null}
        </CardColumns>
        <Modal isOpen={this.state.modal} toggle={this.closeModal} className={this.props.className}>
          <ModalBody>
            {this.state.modal ? <GalleryThumbnail image={this.state.modal.photo}/> :null }
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={this.closeModal}>Cancel</Button>
          </ModalFooter>
        </Modal>

      </Fragment>
    );
  }
}
const mapStateToProps = state => ({
  gallery: state.gallery.gallery,
  user: state.user.user
});
const mapDispatchToProps = dispatch =>({
  fetchGallery: () => dispatch(fetchGallery()),
  deletePhoto: id => dispatch(deletePhoto(id))
});
export default connect(mapStateToProps, mapDispatchToProps) (HomePage);