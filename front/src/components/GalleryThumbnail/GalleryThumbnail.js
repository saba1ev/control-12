import React from 'react';
import {apiURL} from "../../constants";


const styles = {
  width: '150px',
  height: '150px',
  marginRight: '10px'
};

const GalleryThumbnail = props => {
  let image = null;

  if (props.image) {
    image = apiURL + '/uploads/' + props.image;
  }


  return <img src={image} style={styles} className="img-thumbnail" alt="Post Img"/>
};

export default GalleryThumbnail;
