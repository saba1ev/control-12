import React from 'react';
import {NavLink as RouterNavLink} from 'react-router-dom';
import {DropdownItem, DropdownMenu, DropdownToggle, NavLink, UncontrolledDropdown} from "reactstrap";

const UserMenu = ({user, logout}) => (
  <UncontrolledDropdown nav inNavbar>
    <DropdownToggle nav caret>
      Hello, {user.nickname}!
    </DropdownToggle>
    <DropdownMenu right>
      <DropdownItem divider/>
      <DropdownItem onClick={logout}>
        Logout
      </DropdownItem>
      <DropdownItem>
        <NavLink tag={RouterNavLink} to="/user_gallery">My gallery</NavLink>
      </DropdownItem>
      <DropdownItem>
        <NavLink tag={RouterNavLink} to="/add_image">Add image</NavLink>
      </DropdownItem>
      <DropdownItem divider/>
    </DropdownMenu>
  </UncontrolledDropdown>

);

export default UserMenu