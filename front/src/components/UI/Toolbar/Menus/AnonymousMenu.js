import React, {Fragment} from 'react';
import FacebookLogin from "../../../FacebookLogin/FacebookLogin";
import {NavItem, NavLink} from "reactstrap";
import {NavLink as RouterNavLink} from 'react-router-dom';


const AnonymouseMenu = () => (
  <Fragment>
    <NavItem>
      <NavLink tag={RouterNavLink} to="/register" exact>Register</NavLink>
    </NavItem>
    <NavItem>
      <NavLink tag={RouterNavLink} to="/login" exact>Login</NavLink>
    </NavItem>
    <FacebookLogin/>
  </Fragment>
);

export default AnonymouseMenu;