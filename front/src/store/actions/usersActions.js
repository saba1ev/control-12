import {push} from 'connected-react-router';
import {
  LOGIN_USER_FAILURE,
  LOGIN_USER_SUCCESS,
  LOGOUT_USER,
  REGISTER_USER_FAILURE,
  REGISTER_USER_SUCCESS
} from "./ActionsTypes";
import {NotificationManager} from 'react-notifications';
import axios from '../../axios-gallery';

export const registerUserSuccess = payload => {
  return {type: REGISTER_USER_SUCCESS, payload}
};

export const registerUserFailure = error => {
  return {type: REGISTER_USER_FAILURE, error}
};

export const loginUserSuccess = (data) => ({type: LOGIN_USER_SUCCESS, data});

export const loginUserFailure = error => ({type: LOGIN_USER_FAILURE, error});

export const logoutUser = () => {
  return (dispatch, getState) => {
    const token = getState().user.user.token;
    const config = {headers: {'Token': token}};

    return axios.delete('/user/sessions', config).then(
      () => {
        dispatch({type: LOGOUT_USER});
        NotificationManager.success('Logged out');
      },
      error => {
        NotificationManager.error('Could not logout!');
      }
    )
  }
};
export const registerUser = userData => {
  return dispatch => {
    return axios.post('/user', userData)
      .then(response => {
        dispatch(registerUserSuccess(response.data));
        NotificationManager.success('Register successfully');
        dispatch(push('/'));
      }, error => {
        if (error.response) {
          dispatch(registerUserFailure(error.response.data))
        }else {
          dispatch(registerUserFailure({global: 'No connection'}))
        }
      })
  }
};
export const loginUser = userData => {
  return dispatch => {
    return axios.post('user/sessions', userData).then(
      response => {
        dispatch(loginUserSuccess(response.data));
        NotificationManager.success('Logged in successfully');
        dispatch(push('/'));
      },
      error => {
        dispatch(loginUserFailure(error.response.data))
      }
    )
  }
};

export const facebookLogin = userData => {
  return dispatch => {
    return axios.post('/user/facebookLogin', userData)
      .then(response => {
        dispatch(loginUserSuccess(response.data.user));
        NotificationManager.success('Logged in via Facebook');
        dispatch(push('/'))
      }, () => {
        dispatch(loginUserFailure('Login via Facebook failed'))
      });

  }
}