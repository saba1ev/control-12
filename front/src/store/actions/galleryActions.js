import {FETCH_AUTHOR_SUCCESS, FETCH_GALLERY_FAILURE, FETCH_GALLERY_SUCCESS, POST_GALLERY_SUCCESS} from "./ActionsTypes";
import axios from '../../axios-gallery';
import {NotificationManager} from "react-notifications";
import {push} from 'connected-react-router'
export const fetchGallerySuccess = payload => {
  return {type: FETCH_GALLERY_SUCCESS, payload}
};

export const fetchGalleryFailure = failure => {
  return {type: FETCH_GALLERY_FAILURE, failure}
};
export const fetchAuthorSuccess = payload => {
  return {type: FETCH_AUTHOR_SUCCESS, payload}
};

export const postGallerySuccess = () => {
  return {type: POST_GALLERY_SUCCESS}
}

export const fetchGallery = () => {
  return dispatch => {
    axios.get('/gallery')
      .then(response => dispatch(fetchGallerySuccess(response.data)))
      .catch(err => dispatch(fetchGalleryFailure(err)))
  }
};

export const fetchAuthorGallery = (id) => {
  return dispatch => {
    axios.get('/gallery?user=' + id)
      .then(response=> dispatch(fetchAuthorSuccess(response.data)))
  }
};
export const postGallery = (id) => {
  return (dispatch, getState) => {
    const user = getState().user.user;
    return axios.post('/gallery', id, {headers: {'Token': user.token}})
        .then(() => {
          dispatch(postGallerySuccess());
          NotificationManager.success('Photo added');
          dispatch(push('/'))
        })

  }
};

export const deletePhoto = id => {
  return (dispatch, getState) => {
    const user = getState().user.user;
    return axios.delete(`/gallery/${id}`, {headers: {'Token': user.token}})
      .then(()=>dispatch(fetchGallery()))
  }
}
