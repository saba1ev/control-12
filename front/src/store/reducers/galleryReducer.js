import {
  FETCH_AUTHOR_SUCCESS,
  FETCH_GALLERY_FAILURE,
  FETCH_GALLERY_SUCCESS,
  POST_GALLERY_SUCCESS
} from "../actions/ActionsTypes";

const initialState = {
  gallery: null,
  err: null,
  author: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_GALLERY_SUCCESS:
      return {
        ...state,
        gallery: action.payload
      };
    case FETCH_GALLERY_FAILURE:
      return{
        ...state,
        err: action.failure
      };
    case FETCH_AUTHOR_SUCCESS:
      return {
        ...state,
        author: action.payload
      };
    case POST_GALLERY_SUCCESS:
      return {
        ...state
      };

    default:
      return state
  }
};

export default reducer;
