import {
  LOGIN_USER_FAILURE,
  LOGIN_USER_SUCCESS,
  LOGOUT_USER,
  REGISTER_USER_FAILURE,
  REGISTER_USER_SUCCESS
} from "../actions/ActionsTypes";

const initialState = {
  user: null,
  userRegisterError: null,
  loginError: null,
};

const reducer = (state = initialState, action)=>{
  switch (action.type) {
    case LOGOUT_USER:
      return { ...state, user: null};
    case REGISTER_USER_SUCCESS:
      return {...state, userRegisterError: null, user: action.payload};
    case REGISTER_USER_FAILURE:
      return {
        ...state,
        registerError: action.error
      };
    case LOGIN_USER_SUCCESS:
      return {
        ...state,
        user: action.data,
        loginError: null
      };
    case LOGIN_USER_FAILURE:
      return {
        ...state,
        loginError: action.error
      };
    default:
      return state
  }

};

export default reducer;